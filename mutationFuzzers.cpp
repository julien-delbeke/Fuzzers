#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <vector>
#include <unistd.h>



std::vector<std::string> exec(const char*);
void generateTestRuns(char *,long, int , int , double);
std::string randomHexaByte(char);
void writeOutputInFile(char *,long, int);
std::vector<std::string> finalCrashes;


std::vector<std::string> exec(std::string cmd) {
	std::vector<std::string> resultVector;
	std::cout<<cmd<<std::endl;
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd.c_str(), "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
            result += buffer;
        }
        resultVector.push_back(result);
        resultVector.push_back(cmd);
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return resultVector;
}

void generateTestRuns(char * buffer,long size, int numberOfRuns, int maxModifPerRun, double probabilityOfChange ){
	srand (time(NULL));
	std::cout<<"maxModifPerRun "<<maxModifPerRun<<std::endl;
	for (int i = 0;i<numberOfRuns; i++){
		int j = 0;
		bool findError = false;
		while (j<maxModifPerRun and not(findError)){
			for (int k = 4; k<size; k++){
				int random = rand();  
				int MAX = RAND_MAX;;
				double test = (double)random/MAX;
				//std::cout<<"Test:  "<<test<<std::endl;
				//std::cout<<"probabilityOfChange:  "<<probabilityOfChange<<std::endl;
				if (std::islessequal(test,probabilityOfChange)){
					std::string res = randomHexaByte(buffer[k]);
					char result = res[0];
					buffer[k] = result;
					int fileId = i+j+k;
					writeOutputInFile(buffer,size,fileId);
					std::string inputFileName = "input"+std::to_string(fileId);
					std::string outputFileName = "output"+std::to_string(fileId);
					std::string finalName1 = inputFileName + ".img";
					std::string finalName2 = outputFileName + ".img";
					std::string commandToExecute = "./converter.dms "+finalName1+" "+finalName2+" 2>&1";
					std::vector<std::string> execution = exec(commandToExecute);
					std::string MSG = "*** The program has crashed.\n";
					std::cout<<execution[0]<<std::endl;
					if (MSG==execution[0]){
						std::cout<<"le programme a crashé---------------------------------------------------------------"<<std::endl;
						std::cout<<"le fichier qui a crashé: "<<execution[1]<<std::endl;
						std::string moveCommand = "mv "+finalName1+" ../errors";
						system(moveCommand.c_str());
						finalCrashes.push_back(execution[1]);
						findError=true;
					}
					j++;
				}
			}
		}
	}
}

std::string randomHexaByte(char currentByte){
	std::ostringstream os;
	os << std::hex << currentByte;
	std::string currentHexa = os.str();
	bool notequal = true;
	while (notequal){
		int val = rand();
		std::stringstream stream;
		stream << std::hex << val ;
		std::string result( stream.str() );
		std::string onlyOneByte = result.substr(0,2);
	    if (onlyOneByte.compare(currentHexa) != 0){
	    	notequal = false;
	    	int len = onlyOneByte.length();
			std::string newString;
			for(int i=0; i< len; i+=2)
			{
			    std::string byte = onlyOneByte.substr(i,2);
			    char chr = (char) (int)strtol(byte.c_str(), NULL, 16);
			    newString.push_back(chr);
			}
	    	return newString;
	    }
	}    
    
}

void writeOutputInFile(char * buffer,long size, int fileId){
	std::ofstream outFile;
	std::string fileName = "input"+ std::to_string(fileId);
	std::string finalName = fileName + ".img";
	
	outFile.open(finalName, std::ios::out | std::ios::binary);
	if (outFile.is_open()){
		outFile.write(buffer, size);
		outFile.close();
	}
	else{
		std::cout<<"erreur openinf file"<<std::endl;
	}	
}
int main(int argc, char *argv[]){
	char * buffer;
	long size;
	std::ifstream file(argv[1], std::ios::in|std::ios::binary|std::ios::ate);
	size = file.tellg();
	file.seekg(0, std::ios::beg);
	buffer = new char [size];
	file.read(buffer,size);
	file.close();
	int arg2 = strtol(argv[2], NULL, 10);
	int arg3 = strtol(argv[3], NULL, 10);
	double rate = atof(argv[4]);
	generateTestRuns(buffer,size,arg2, arg3, rate);
	std::cout<<"Size Vectorfinal-------"<<finalCrashes.size()<<std::endl;
	for (std::vector<std::string>::const_iterator i = finalCrashes.begin(); i != finalCrashes.end(); ++i)
    	std::cout << *i << std::endl;
	return 0;
}

