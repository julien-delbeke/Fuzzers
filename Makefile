tool:
	g++-6 mutationFuzzer.cpp -o myfuzzer

clean: 
	rm input*
	rm output*
	rm myfuzzer